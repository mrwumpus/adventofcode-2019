defmodule Intcode2 do
  @moduledoc """
  Second Day of AOC
  """
  defp str_to_opcodes(str),
    do: String.trim(str) |> String.split(",") |> Enum.map(&String.to_integer/1)

  defp opcodes_to_str(opcodes), do: Enum.join(opcodes, ",")

  defp get_parameters(opcodes, offset),
    do:
      Enum.slice(opcodes, offset + 1, 3)
      |> (fn
            [xoffset, yoffset, result_offset] ->
              {Enum.at(opcodes, xoffset), Enum.at(opcodes, yoffset), result_offset}
          end).()

  defp execute_operation(opcodes, offset, op),
    do:
      get_parameters(opcodes, offset)
      |> (fn {x, y, result_offset} ->
            List.replace_at(opcodes, result_offset, op.(x, y))
          end).()
      |> next_operation(offset + 4)

  defp do_operation(1, opcodes, offset),
    do: execute_operation(opcodes, offset, fn x, y -> x + y end)

  defp do_operation(2, opcodes, offset),
    do: execute_operation(opcodes, offset, fn x, y -> x * y end)

  defp do_operation(99, opcodes, _offset), do: opcodes

  defp do_operation(operation, _opcodes, offset),
    do: raise("Invalid Operation: #{operation} found at #{offset}")

  defp next_operation(opcodes, offset),
    do: Enum.at(opcodes, offset) |> do_operation(opcodes, offset)

  defp restore(opcodes, noun, verb),
    do:
      List.replace_at(opcodes, 1, noun)
      |> List.replace_at(2, verb)

  def run_code(opcode_string, noun \\ -1, verb \\ -1),
    do:
      str_to_opcodes(opcode_string)
      |> (fn opcodes, noun, verb ->
            if noun > -1 && verb > -1, do: restore(opcodes, noun, verb), else: opcodes
          end).(noun, verb)
      |> next_operation(0)
      |> opcodes_to_str()

  def get_num(opcode_string, position), do: str_to_opcodes(opcode_string) |> Enum.at(position)

  def search_result(opcode_string, result),
    do:
      for(
        noun <- 0..99,
        verb <- 0..99,
        get_num(run_code(opcode_string, noun, verb), 0) == result,
        do: {noun, verb}
      )
end
