defmodule IoAgent do
  use Agent

  @moduledoc """
  Agent holding current input and output devices
  """

  def start_link(),
    do: Agent.start_link(fn -> %{:stdin => :stdio, :stdout => :stdio} end, name: __MODULE__)

  def stdin(), do: Agent.get(__MODULE__, fn map -> Map.get(map, :stdin) end)
  def set_stdin(device), do:
    Agent.update(__MODULE__, fn map -> Map.put(map, :stdin, device) end)

  def stdout(), do: Agent.get(__MODULE__, fn map -> Map.get(map, :stdout) end)
  def set_stdout(device), do:
    Agent.update(__MODULE__, fn map -> Map.put(map, :stdout, device) end)

end
