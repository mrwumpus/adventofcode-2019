defmodule Bugs do
  @bug "#"
  @empty "."

  defp get(_grid, width, height, x, y) when x < 0 or x >= width or y < 0 or y >= height,
    do: @empty

  defp get(grid, width, _height, x, y), do: String.at(grid, y * width + x)

  defp surrounding_bug_count(grid, width, height, x, y) do
    n = get(grid, width, height, x, y - 1)
    e = get(grid, width, height, x + 1, y)
    s = get(grid, width, height, x, y + 1)
    w = get(grid, width, height, x - 1, y)

    Enum.filter([n, s, e, w], fn sp -> sp == @bug end)
    |> Enum.count()
  end

  defp result(@bug, surrounding_bug_count) when surrounding_bug_count != 1, do: @empty

  defp result(@empty, surrounding_bug_count)
       when surrounding_bug_count > 0 and surrounding_bug_count < 3,
       do: @bug

  defp result(sp, _), do: sp

  defp calc_biodiversity([], _value, total), do: total

  defp calc_biodiversity([p | rest], value, total) do
    mult = if p == @bug, do: 1, else: 0
    calc_biodiversity(rest, value * 2, total + mult * value)
  end

  def biodiversity(grid) do
    calc_biodiversity(String.graphemes(grid), 1, 0)
  end

  def step(grid, width, height) do
    for y <- 0..(width - 1),
        x <- 0..(height - 1) do
      space = get(grid, width, height, x, y)
      surrounding_bug_count = surrounding_bug_count(grid, width, height, x, y)
      result(space, surrounding_bug_count)
    end
    |> List.to_string()
  end

  defp do_find_dup(grid, width, height, history) do
    bio = biodiversity(grid)

    if MapSet.member?(history, bio),
      do: grid,
      else:
        do_find_dup(
          step(grid, width, height),
          width,
          height,
          MapSet.put(history, bio)
        )
  end

  def find_dup(grid, width, height) do
    do_find_dup(grid, width, height, %MapSet{})
  end

  def print(grid, width) do
    str =
      String.graphemes(grid)
      |> Enum.chunk_every(width)
      |> Enum.join("\n")

    IO.puts("#{str}\n")
    grid
  end
end
