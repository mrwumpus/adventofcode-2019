defmodule Fft do
  # defp pattern(repeat) do
  # base = List.duplicate(0, repeat)
  # base = base ++ List.duplicate(1, repeat)
  # base = base ++ List.duplicate(0, repeat)
  # base ++ List.duplicate(-1, repeat)
  # end

  defp pattern_at(position, idx) do
    pattern = rem(div(idx - position + 1, position), 4)

    cond do
      idx + 1 < position -> 0
      pattern == 0 -> 1
      pattern == 1 -> 0
      pattern == 2 -> -1
      pattern == 3 -> 0
    end
  end

  def process_signal(input_str, phases) do
    String.graphemes(input_str)
    |> Enum.map(&String.to_integer/1)
    |> do_process_signal(phases)
    |> Enum.join()
  end

  defp do_process_signal(input, 0), do: input

  defp do_process_signal(input, phases) do
    #IO.puts("\nPhase #{phases}...")
    #IO.write(" pos: ")
    1..length(input)
    |> Enum.map(fn position ->
      process_position(input, position)
    end)
    |> do_process_signal(phases - 1)
  end

  defp process_position(input, position) do
    #IO.write(" #{position}")
    input
    |> Stream.with_index()
    |> Enum.reduce(0, fn {p, i}, sum -> p * pattern_at(position, i) + sum end)
    |> (fn v -> abs(rem(v, 10)) end).()
  end
end
