defmodule Util do
  def input_day(day) do
    input_file = "test/day#{day}/input"

      case File.read(input_file) do
        {:ok, body} ->
          body

        {:error, reason} ->
          IO.puts("Oops -- #{reason}")
          :error
      end
  end
end
