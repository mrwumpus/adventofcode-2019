defmodule BeamTracker do
  def find_beam(check_point, x, y) do
    if check_point.(x, y) == 1 do
      #IO.puts("Beam start at (#{x},#{y})")
      x
    else
      find_beam(check_point, x + 1, y)
    end
  end

  def end_beam(check_point, x, y) do
    if check_point.(x, y) == 0 do
      #IO.puts("Beam end at (#{x - 1},#{y})")
      x - 1
    else
      end_beam(check_point, x + 1, y)
    end
  end

  def check_offsets_within(check_point, offset, x, y) do
    check_point.(x - offset, y) == 1 and
      check_point.(x - offset, y + offset) == 1
  end

  def search_offset_within(check_point, offset, x, y, px) do
    if check_offsets_within(check_point, offset, x, y),
      do: search_offset_within(check_point, offset, x - 1, y, x),
      else: px
  end

  def search_for_room(check_point, offset, x, y) do
    #IO.puts("Searching row: #{y}")
    #start = find_beam(check_point, x, y)
    line_end = end_beam(check_point, x, y)
    found = search_offset_within(check_point, offset, line_end, y, nil)

    if is_nil(found),
      do: search_for_room(check_point, offset, line_end, y + 1),
      else: {found - offset, y}
  end
end
