defmodule Monitor do
  use GenServer

  def start_link() do
    GenServer.start_link(__MODULE__, %{screen: %{}, input: nil, ball: nil, paddle: nil})
  end

  def screen(pid) do
    GenServer.call(pid, :screen)
  end

  def ball_paddle_blocks(pid) do
    GenServer.call(pid, :ball_paddle_blocks)
  end

  @impl true
  def init(state) do
    {:ok, state}
  end

  @impl true
  def handle_call(:screen, _from, state) do
    %{:screen => screen} = state
    {:reply, screen, state}
  end

  @impl true
  def handle_call(:ball_paddle_blocks, _from, state) do
    %{:screen => screen, :ball => ball, :paddle => paddle} = state

    blocks =
      screen
      |> Map.values()
      |> Enum.filter(fn v -> v == 2 end)
      |> Enum.count()

    {:reply, {ball, paddle, blocks}, state}
  end

  @impl true
  def handle_info(
        message,
        %{:screen => screen, :input => nil, :ball => ball, :paddle => paddle}
      ) do
    x = String.to_integer(message)
    new_state = %{screen: screen, input: {x}, ball: ball, paddle: paddle}
    {:noreply, new_state}
  end

  @impl true
  def handle_info(
        message,
        %{:screen => screen, :input => {x}, :ball => ball, :paddle => paddle}
      ) do
    y = String.to_integer(message)
    new_state = %{screen: screen, input: {x, y}, ball: ball, paddle: paddle}
    {:noreply, new_state}
  end

  @impl true
  def handle_info(
        message,
        %{:screen => screen, :input => {x, y}, :ball => ball, :paddle => paddle}
      ) do
    tile_id = String.to_integer(message)
    ball = if tile_id == 4, do: {x, y}, else: ball
    paddle = if tile_id == 3, do: x, else: paddle

    new_state = %{
      screen: Map.put(screen, {x, y}, tile_id),
      input: nil,
      ball: ball,
      paddle: paddle
    }

    {:noreply, new_state}
  end
end
