defmodule SpaceImage do
  @moduledoc """
  Day 8 - Space image
  """

  defp parse_layer("", _, layers), do: layers |> Enum.reverse()

  defp parse_layer(pixels, layer_length, layers) do
    {layer, rest} = String.split_at(pixels, layer_length)
    parse_layer(rest, layer_length, [layer | layers])
  end

  def layers(config_pid) do
    {pixels, width, height} = SpaceImageConfig.get(config_pid)
    layer_length = width * height
    parse_layer(pixels, layer_length, [])
  end

  defp count_digit([], _), do: 0

  defp count_digit([h | t], digit) do
    match_add = if h == digit, do: 1, else: 0

    match_add + count_digit(t, digit)
  end

  def count_in_layer(layer, digit),
    do: count_digit(String.graphemes(layer), Integer.to_string(digit))

  defp search_layers_for_fewest([], _, _, layer), do: layer

  defp search_layers_for_fewest([h | t], digit, nil, nil) do
    count = count_in_layer(h, digit)
    search_layers_for_fewest(t, digit, count, h)
  end

  defp search_layers_for_fewest([h | t], digit, previous_min, layer) do
    count = count_in_layer(h, digit)

    if count < previous_min,
      do: search_layers_for_fewest(t, digit, count, h),
      else: search_layers_for_fewest(t, digit, previous_min, layer)
  end

  def layer_with_fewest(config_pid, digit) do
    layers = layers(config_pid)
    search_layers_for_fewest(layers, digit, nil, nil)
  end

  defp reduce_pixel(_, 0), do: 0
  defp reduce_pixel(_, 1), do: 1
  defp reduce_pixel(0, 2), do: 0
  defp reduce_pixel(1, 2), do: 1
  defp reduce_pixel(2, 2), do: 2

  defp merge_layers(config_pid) do
    layers(config_pid)
    |> Enum.map(fn layer ->
      String.graphemes(layer)
      |> Enum.map(&String.to_integer/1)
    end)
    |> Enum.zip()
    |> Enum.map(fn pixel_stack ->
      Tuple.to_list(pixel_stack)
      |> Enum.reduce(&reduce_pixel/2)
    end)
  end

  defp render_pixel(0), do: " "
  defp render_pixel(1), do: "#"
  defp render_pixel(2), do: "_"

  def render_message(config_pid) do
    {_, width, _} = SpaceImageConfig.get(config_pid)

    merge_layers(config_pid)
    |> Enum.chunk_every(width)
    |> Enum.map(fn row ->
      Enum.map(row, &render_pixel/1)
      |> Enum.join()
    end)
    |> Enum.join("\n")
  end
end

defmodule SpaceImageConfig do
  use Agent

  @moduledoc """
  Holds the config values for a SpaceImage
  """

  def start_link(pixels, width, height), do: Agent.start_link(fn -> {pixels, width, height} end)

  def get(pid), do: Agent.get(pid, fn config -> config end)
end
