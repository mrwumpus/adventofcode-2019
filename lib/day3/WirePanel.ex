defmodule WirePanel do
  @moduledoc """
  Day 3 wire panel
  """
  defp parse_vector(vector),
    do:
      {String.first(vector),
       String.slice(vector, 1, String.length(vector)) |> String.to_integer()}

  defp run_segment(p, vector) do
    {direction, length} = parse_vector(vector)
    {x, y} = p

    end_point =
      case direction do
        "U" -> {x, y + length}
        "D" -> {x, y - length}
        "R" -> {x + length, y}
        "L" -> {x - length, y}
      end

    {p, end_point}
  end

  defp run_wire([], vector), do: [run_segment({0, 0}, vector)]

  defp run_wire(wire, vector) do
    [{_, end_point} | _] = wire
    [run_segment(end_point, vector) | wire]
  end

  defp wire(wire_string),
    do:
      String.trim(wire_string)
      |> String.split(",")
      |> Enum.reduce([], fn vector, wire -> run_wire(wire, vector) end)
      |> Enum.reverse()

  defp manhattan({x, y}), do: abs(x) + abs(y)

  defp between(x, s, e), do: x >= min(s, e) and x <= max(s, e)

  defp intersect(
         {{sx1, sy1}, {ex1, ey1}},
         {{sx2, sy2}, {ex2, ey2}}
       ) do
    cond do
      between(sx2, sx1, ex1) and between(sy1, sy2, ey2) ->
        {sx2, sy1}

      between(sx1, sx2, ex2) and between(sy2, sy1, ey1) ->
        {sx1, sy2}

      true ->
        nil
    end
  end

  defp steps({x1, y1}, {x2, y2}), do: manhattan({x1 - x2, y1 - y2})

  defp point_on_segment({px, py}, {{x1, y1}, {x2, y2}}),
    do:
      (between(px, x1, x2) and py == y1 and y1 == y2) or
        (between(py, y1, y2) and px == x1 and x1 == x2)

  defp steps_to_point([], _, _), do: nil

  defp steps_to_point([segment | rest], pt, steps) do
    {p1, p2} = segment

    if point_on_segment(pt, segment),
      do: steps + steps(p1, pt),
      else: steps_to_point(rest, pt, steps + steps(p1, p2))
  end

  def intersections(wire1, wire2),
    do:
      for(
        seg1 <- wire1,
        seg2 <- wire2,
        intersect = intersect(seg1, seg2),
        {0, 0} != intersect,
        do: intersect
      )

  def minimum_manhattan([w1_string, w2_string]) do
    {w1, w2} = {wire(w1_string), wire(w2_string)}

    intersections(w1, w2)
    |> Enum.map(&manhattan/1)
    |> Enum.min(fn -> -1 end)
  end

  def minimum_intersection_steps([w1_string, w2_string]) do
    {w1, w2} = {wire(w1_string), wire(w2_string)}

    for(
      pt <- intersections(w1, w2),
      w1_steps = steps_to_point(w1, pt, 0),
      w2_steps = steps_to_point(w2, pt, 0),
      do: w1_steps + w2_steps
    )
    |> Enum.min()
  end

  # defp point_str(nil), do: "nowhere"
  # defp point_str({x, y}), do: "(#{x}, #{y})"
  # defp segment_str({p1, p2}), do: "#{point_str(p1)} -- #{point_str(p2)}"
end
