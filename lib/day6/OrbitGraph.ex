defmodule OrbitGraph do
  @moduledoc """
  Day 5, Orbit graph
  """
  defp split_token(orbit_token) do
    orbit_token |> String.trim() |> String.split(")")
  end

  def walk_orbits(nil, _, steps), do: steps

  def walk_orbits(center, orbits, steps) do
    Map.get(orbits, center, [])
    |> Enum.map(fn c -> walk_orbits(c, orbits, steps + 1) end)
    |> Enum.sum()
    |> (fn sum -> sum + steps end).()
  end

  def walk_orbits() do
    walk_orbits("COM", Orbits.orbits(), 0)
  end

  defp find_path(dest, dest, _), do: []

  defp find_path(start, dest, orbits) do
    path =
      Map.get(orbits, start, [])
      |> Enum.map(fn s -> find_path(s, dest, orbits) end)
      |> Enum.find(&is_list/1)

    if is_list(path),
      do: [start | path],
      else: nil
  end

  def find_path(dest) do
    find_path("COM", dest, Orbits.orbits())
  end

  def count_shared_ancestry(path1, path2) do
    Enum.zip(path1, path2)
    |> Enum.filter(fn {f, s} -> f == s end)
    |> length()
  end

  def steps_between(start_path, finish_path) when is_list(start_path) and is_list(finish_path) do
    shared = count_shared_ancestry(start_path, finish_path)
    start_path
    |> Enum.slice(shared, length(start_path))
    |> Enum.reverse()
    |> Enum.concat(
      finish_path
      |> Enum.slice(shared - 1, length(finish_path))
    )
  end

  def steps_between(nil, _), do: []
  def steps_between(_, nil), do: []

  def steps_between(start, finish) do
    steps_between(
      find_path(start),
      find_path(finish)
    )
  end

  def add_orbit(orbit_token) do
    split_token(orbit_token) |> Orbits.add_orbit()
  end
end

defmodule Orbits do
  use Agent

  @moduledoc """
  Orbits
  """
  def start_link() do
    Agent.start_link(fn -> %{} end, name: __MODULE__)
  end

  def add_orbit([center, orbiter]) do
    Agent.update(__MODULE__, fn map ->
      {_, new_map} =
        Map.get_and_update(map, center, fn
          x when is_nil(x) -> {x, [orbiter]}
          x -> {x, [orbiter | x]}
        end)

      new_map
    end)
  end

  def orbits() do
    Agent.get(__MODULE__, fn map -> map end)
  end
end
