defmodule Intcode5 do
  @moduledoc """
  Second Day of AOC
  """
  defp str_to_opcodes(str),
    do: String.trim(str) |> String.split(",") |> Enum.map(&String.to_integer/1)

  defp opcodes_to_str(opcodes), do: Enum.join(opcodes, ",")

  defp full_modes(modes, param_count) when param_count > length(modes),
    do: full_modes(List.insert_at(modes, -1, 0), param_count)

  defp full_modes(modes, _), do: modes

  defp params(opcodes, offset, param_count), do: Enum.slice(opcodes, offset + 1, param_count)

  defp get_parameters(params, opcodes, modes),
    do:
      params
      |> Enum.zip(full_modes(modes, length(params)))
      |> Enum.map(fn {num, mode} ->
        case mode do
          0 -> Enum.at(opcodes, num)
          1 -> num
        end
      end)

  defp execute_result_operation(opcodes, offset, modes, param_count, op) do
    params = params(opcodes, offset, param_count)
    result_offset = List.last(params)
    params = Enum.slice(params, 0, param_count - 1)

    result =
      get_parameters(params, opcodes, modes)
      |> op.()

    List.replace_at(opcodes, result_offset, result)
    |> next_operation(offset + param_count + 1)
  end

  defp do_jump_check_op(modes, opcodes, offset, check_fn) do
    [check, destination] =
      params(opcodes, offset, 2)
      |> get_parameters(opcodes, modes)

    next_offset = if check_fn.(check), do: destination, else: offset + 3
    next_operation(opcodes, next_offset)
  end

  defp do_operation([1 | modes], opcodes, offset),
    do: execute_result_operation(opcodes, offset, modes, 3, fn [x, y] -> x + y end)

  defp do_operation([2 | modes], opcodes, offset),
    do: execute_result_operation(opcodes, offset, modes, 3, fn [x, y] -> x * y end)

  defp do_operation([3 | _], opcodes, offset),
    do:
      execute_result_operation(opcodes, offset, [], 1, fn [] ->
        IO.gets(IoAgent.stdin(), ">> ")
        |> String.trim()
        |> String.to_integer()
      end)

  defp do_operation([4 | modes], opcodes, offset) do
    params = params(opcodes, offset, 1)
    [result] = get_parameters(params, opcodes, modes)
    IO.puts(IoAgent.stdout(), "#{result}")
    next_operation(opcodes, offset + 2)
  end

  defp do_operation([5 | modes], opcodes, offset),
    do: do_jump_check_op(modes, opcodes, offset, fn check -> check != 0 end)

  defp do_operation([6 | modes], opcodes, offset),
    do: do_jump_check_op(modes, opcodes, offset, fn check -> check == 0 end)

  defp do_operation([7 | modes], opcodes, offset),
    do:
      execute_result_operation(opcodes, offset, modes, 3, fn
        [x, y] when x < y -> 1
        _ -> 0
      end)

  defp do_operation([8 | modes], opcodes, offset),
    do:
      execute_result_operation(opcodes, offset, modes, 3, fn
        [x, y] when x == y -> 1
        _ -> 0
      end)

  defp do_operation([99 | _], opcodes, _), do: opcodes

  defp do_operation([operation | _], _, offset),
    do: raise("Invalid Operation: #{operation} found at #{offset}")

  defp split_by_divisor(number, divisor) do
    code = rem(number, divisor)
    rest = div(number, divisor)
    {code, rest}
  end

  defp split_opcode(opcode, result \\ [])
  defp split_opcode(0, result), do: Enum.reverse(result)

  defp split_opcode(opcode, []) do
    {code, rest} = split_by_divisor(opcode, 100)
    split_opcode(rest, [code])
  end

  defp split_opcode(opcode, result) do
    {code, rest} = split_by_divisor(opcode, 10)
    split_opcode(rest, [code | result])
  end

  defp next_operation(opcodes, offset) do
    Enum.at(opcodes, offset)
    |> split_opcode()
    |> do_operation(opcodes, offset)
  end

  def run_code(opcode_string),
    do:
      str_to_opcodes(opcode_string)
      |> next_operation(0)
      |> opcodes_to_str()
end
