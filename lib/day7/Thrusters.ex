defmodule Thrusters do
  @moduledoc """
  Day 7 challenge
  """
  defp check_with_io(input, opcodes) do
    {:ok, result} =
      StringIO.open("", fn o_pid ->
        IoAgent.set_stdout(o_pid)

        StringIO.open(input, fn i_pid ->
          IoAgent.set_stdin(i_pid)
          Intcode5.run_code(opcodes)
        end)

        StringIO.contents(o_pid)
        |> (fn {_, result} -> result end).()
        |> String.trim()
        |> String.split()
        |> List.last()
        |> String.to_integer()
      end)

    result
  end

  def run_amp(phase_inputs, int_code) do
    run_amp(0, phase_inputs, int_code)
  end

  def run_amp(last_result, [], _), do: last_result

  def run_amp(last_result, [h | t], int_code) do
    "#{h}\n#{last_result}"
    |> check_with_io(int_code)
    |> run_amp(t, int_code)
  end

  def feedback_amp(phase_inputs, int_code) do
    100
  end
end
