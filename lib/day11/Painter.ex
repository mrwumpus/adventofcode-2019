defmodule Painter do
  use GenServer

  def start_link(start_color \\ 0) do
    GenServer.start_link(__MODULE__, %{
      default_color: 0,
      # {x, y, orientation: 0N, 1E, 2S, 3W}
      current: {0, 0, 0},
      nodes: %{{0, 0} => start_color},
      color_command: nil,
      output_pid: nil
    })
  end

  def move(pid, color, rotation) do
    rot = if rotation == 0, do: -1, else: rotation
    GenServer.call(pid, {:move, {color, rot}})
  end

  def set_ouput_pid(pid, out_pid) do
    GenServer.call(pid, {:out_pid, out_pid})
  end

  def complete(pid) do
    state = GenServer.call(pid, :state)
    GenServer.stop(pid)
    state
  end

  @impl true
  def init(initial) do
    {:ok, initial}
  end

  def execute_move(state, color, rotation) do
    rot = if rotation == 0, do: -1, else: rotation

    state
    |> paint_cell(color)
    |> move_painter(rot)
    |> send_current_color()
  end

  def send_current_color(state) do
    %{:output_pid => out_pid} = state
    if out_pid, do: send(out_pid, node_color(state))
    state
  end

  @impl true
  def handle_call({:move, {color, rotation}}, _from, state) do
    new_state = execute_move(state, color, rotation)
    {:reply, node_color(new_state), new_state}
  end

  @impl true
  def handle_call(:state, _from, state) do
    {:reply, state, state}
  end

  @impl true
  def handle_call({:out_pid, out_pid}, _from, state) do
    new_state =
      Map.put(state, :output_pid, out_pid)
      |> send_current_color()

    {:reply, new_state, new_state}
  end

  @impl true
  def handle_call(request, _from, state) do
    IO.puts("handle call: #{request}")
    {:noreply, state}
  end

  @impl true
  def handle_info(request, state) do
    %{:color_command => col_cmd} = state

    new_state =
      if col_cmd,
        do:
          Map.put(state, :color_command, nil)
          |> execute_move(String.to_integer(col_cmd), String.to_integer(request)),
        else: Map.put(state, :color_command, request)

    {:noreply, new_state}
  end

  defp add_or_replace_node(nodes, node, color) do
    Map.put(nodes, node, color)
  end

  defp paint_cell(state, color) do
    %{:current => {x, y, _}, :nodes => nodes} = state
    Map.put(state, :nodes, add_or_replace_node(nodes, {x, y}, color))
  end

  def move_painter(state, rot) do
    %{:current => {x, y, orientation}} = state
    new_orientation = rem(orientation + 4 + rot, 4)
    Map.put(state, :current, walk(x, y, new_orientation))
  end

  def node_color(%{:nodes => nodes, :current => {x, y, _}, :default_color => default_color}) do
    Map.get(nodes, {x, y}, default_color)
  end

  def walk(x, y, 0), do: {x, y + 1, 0}
  def walk(x, y, 1), do: {x + 1, y, 1}
  def walk(x, y, 2), do: {x, y - 1, 2}
  def walk(x, y, 3), do: {x - 1, y, 3}
end
