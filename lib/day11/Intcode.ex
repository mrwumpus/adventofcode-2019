defmodule Intcode11 do
  defp execute_result_operation(config_id, modes, param_count, op) do
    params = IntcodeState11.params(config_id, param_count, modes)
    result_param = List.last(params)

    result =
      Enum.slice(params, 0, param_count - 1)
      |> Enum.map(fn param ->
        IntcodeState11.read(config_id, param)
      end)
      |> op.()

    IntcodeState11.write(config_id, result_param, result)
    next_operation(config_id, param_count + 1)
  end

  defp do_jump_check_op(modes, config_id, check_fn) do
    [check, destination] =
      IntcodeState11.params(config_id, 2, modes)
      |> Enum.map(fn param ->
        IntcodeState11.read(config_id, param)
      end)

    next_offset =
      if check_fn.(check),
        do: IntcodeState11.jmp(config_id, destination),
        else: 3

    next_operation(config_id, next_offset)
  end

  defp input() do
    receive do
      input -> input
    end
  end

  defp do_operation([1 | modes], config_id),
    do: execute_result_operation(config_id, modes, 3, fn [x, y] -> x + y end)

  defp do_operation([2 | modes], config_id),
    do: execute_result_operation(config_id, modes, 3, fn [x, y] -> x * y end)

  defp do_operation([3 | modes], config_id),
    do: execute_result_operation(config_id, modes, 1, fn [] -> input() end)

  defp do_operation([4 | modes], config_id) do
    [result] =
      IntcodeState11.params(config_id, 1, modes)
      |> Enum.map(fn param ->
        IntcodeState11.read(config_id, param)
      end)

    {_, _, _, output_pid} = IntcodeState11.config(config_id)

    #IO.write(result)

    send(output_pid, "#{result}")
    next_operation(config_id, 2)
  end

  defp do_operation([5 | modes], config_id),
    do: do_jump_check_op(modes, config_id, fn check -> check != 0 end)

  defp do_operation([6 | modes], config_id),
    do: do_jump_check_op(modes, config_id, fn check -> check == 0 end)

  defp do_operation([7 | modes], config_id),
    do:
      execute_result_operation(config_id, modes, 3, fn
        [x, y] when x < y -> 1
        _ -> 0
      end)

  defp do_operation([8 | modes], config_id),
    do:
      execute_result_operation(config_id, modes, 3, fn
        [x, y] when x == y -> 1
        _ -> 0
      end)

  defp do_operation([9 | modes], config_id) do
    [result] =
      IntcodeState11.params(config_id, 1, modes)
      |> Enum.map(fn param ->
        IntcodeState11.read(config_id, param)
      end)

    IntcodeState11.add_base(config_id, result)
    next_operation(config_id, 2)
  end

  defp do_operation([99 | _], config_id), do: config_id

  defp do_operation([operation | _], config_id) do
    {_, offset, _, _} = IntcodeState11.config(config_id)
    raise("Invalid Operation: #{operation} found at #{offset}")
  end

  defp next_operation(config_id, inc) do
    IntcodeState11.next_operation(config_id, inc)
    |> do_operation(config_id)
  end

  def run_code(config_id) do
    next_operation(config_id, 0)
    IntcodeState11.intcode(config_id)
  end
end

defmodule IntcodeState11 do
  use Agent

  defp str_to_opcodes(str),
    do: String.trim(str) |> String.split(",") |> Enum.map(&String.to_integer/1)

  defp opcodes_to_str(intcode), do: Enum.join(intcode, ",")

  def start_link(opcode_str, out_pid) do
    Agent.start_link(fn ->
      intcode = str_to_opcodes(opcode_str)
      {intcode, 0, 0, out_pid}
    end)
  end

  def config(config_id) do
    Agent.get(config_id, fn config -> config end)
  end

  def intcode(config_id) do
    {intcode, _, _, _} = config(config_id)
    opcodes_to_str(intcode)
  end

  def add_offset(config_id, amount) do
    Agent.update(config_id, fn {intcode, offset, base, output} ->
      {intcode, offset + amount, base, output}
    end)
  end

  def add_base(config_id, amount) do
    Agent.update(config_id, fn {intcode, offset, base, output} ->
      {intcode, offset, base + amount, output}
    end)
  end

  defp grow_to(intcode, offset) when length(intcode) > offset, do: intcode

  defp grow_to(intcode, offset) do
    diff = offset - length(intcode) + 1
    Enum.concat(intcode, List.duplicate(0, diff))
  end

  def write(config_id, {result_offset, 2}, result) do
    Agent.update(config_id, fn {intcode, offset, base, output} ->
      new_code =
        grow_to(intcode, base + result_offset)
        |> List.replace_at(base + result_offset, result)

      {new_code, offset, base, output}
    end)
  end

  def write(config_id, {result_offset, _mode}, result) do
    Agent.update(config_id, fn {intcode, offset, base, output} ->
      new_code =
        grow_to(intcode, result_offset)
        |> List.replace_at(result_offset, result)

      {new_code, offset, base, output}
    end)
  end

  defp split_by_divisor(number, divisor) do
    code = rem(number, divisor)
    rest = div(number, divisor)
    {code, rest}
  end

  defp split_opcode(opcode, result \\ [])
  defp split_opcode(0, result), do: Enum.reverse(result)

  defp split_opcode(opcode, []) do
    {code, rest} = split_by_divisor(opcode, 100)
    split_opcode(rest, [code])
  end

  defp split_opcode(opcode, result) do
    {code, rest} = split_by_divisor(opcode, 10)
    split_opcode(rest, [code | result])
  end

  def jmp(config_id, dest) do
    Agent.update(config_id, fn {intcode, _, base, output} ->
      {intcode, dest, base, output}
    end)

    0
  end

  def next_operation(config_id, inc \\ 0) do
    if inc > 0, do: add_offset(config_id, inc)
    {intcode, offset, _, _} = config(config_id)

    Enum.at(intcode, offset)
    |> split_opcode()
  end

  def read(config_id, {num, 0}) do
    {intcode, _, _, _} = config(config_id)
    Enum.at(intcode, num, 0)
  end

  def read(_, {num, 1}), do: num

  def read(config_id, {num, 2}) do
    {intcode, _, base, _} = config(config_id)
    Enum.at(intcode, base + num, 0)
  end

  def params(config_id, param_count, modes) do
    {intcode, offset, _, _} = config(config_id)

    Enum.slice(intcode, offset + 1, param_count)
    |> Enum.zip(full_modes(modes, param_count))
  end

  defp full_modes(modes, param_count) when param_count > length(modes),
    do: full_modes(List.insert_at(modes, -1, 0), param_count)

  defp full_modes(modes, _), do: modes
end
