defmodule Moons do
  use GenServer

  defp parse_moons_string(moons_string) do
    String.trim(moons_string)
    |> String.split("\n")
    |> Enum.map(fn moon_line ->
      [x, y, z] =
        Regex.run(~r/<x=(-?\d+), y=(-?\d+), z=(-?\d+)>/, moon_line)
        |> Enum.slice(1, 3)
        |> Enum.map(&String.to_integer/1)

      {{x, y, z}, {0, 0, 0}}
    end)
  end

  def start_link(moons_string) do
    moons = parse_moons_string(moons_string)

    GenServer.start_link(__MODULE__, %{
      moons: moons
    })
  end

  def simulate(pid, steps) do
    GenServer.call(pid, {:simulate, steps})
  end

  defp match(_, initial, initial, count), do: count
  defp match(pid, initial, _, count) do
    if rem(count, 1_000_000) == 0, do: IO.puts("Iteration: #{count}")
    match(pid, initial, simulate(pid, 1), count + 1)
  end

  def find_loop(pid) do
    s1 = state(pid)
    match(pid, s1, nil, 0)
  end

  def state(pid) do
    GenServer.call(pid, :state)
  end

  @impl true
  def init(initial) do
    {:ok, initial}
  end

  @impl true
  def handle_call(:state, _from, state) do
    {:reply, state, state}
  end

  @impl true
  def handle_call({:simulate, steps}, _from, state) do
    new_state = step_state(state, steps)
    {:reply, new_state, new_state}
  end

  defp calc_change(x, y, dx) do
    cond do
      x < y -> dx + 1
      x > y -> dx - 1
      true -> dx
    end
  end

  defp calc_change(
         {{x, y, z}, {dx, dy, dz}},
         {{x2, y2, z2}, _v2}
       ) do
    {{x, y, z}, {calc_change(x, x2, dx), calc_change(y, y2, dy), calc_change(z, z2, dz)}}
  end

  defp apply_gravity(moons) do
    Enum.map(moons, fn target ->
      Enum.reduce(moons, target, fn moon, acc ->
        calc_change(acc, moon)
      end)
    end)
  end

  defp apply_velocity(moons) do
    Enum.map(moons, fn {{x, y, z}, {dx, dy, dz}} ->
      {{x + dx, y + dy, z + dz}, {dx, dy, dz}}
    end)
  end

  defp step_state(state, 0), do: state

  defp step_state(%{:moons => moons}, remaining) do
    new_moons =
      apply_gravity(moons)
      |> apply_velocity()

    step_state(%{moons: new_moons}, remaining - 1)
  end
end
