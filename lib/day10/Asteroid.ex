defmodule Asteroid do
  defp locate_asteroids(field) do
    String.split(field)
    |> Enum.with_index()
    |> Enum.flat_map(fn {line, i} ->
      String.graphemes(line)
      |> Enum.with_index()
      |> Enum.filter(fn {c, _} -> c == "#" end)
      |> Enum.map(fn {_, j} -> {j, i} end)
    end)
  end

  defp between(x, s, e), do: x >= min(s, e) and x <= max(s, e)

  defp possible_obstructions(_fst, _lst, [], possibles), do: possibles

  defp possible_obstructions(p1, p2, [h | t], possibles)
       when p1 == h or p2 == h,
       do: possible_obstructions(p1, p2, t, possibles)

  defp possible_obstructions(p1, p2, [{xp, yp} | t], possibles) do
    [{x1, y1}, {x2, y2}] = [p1, p2]

    new_possibles =
      if between(xp, x1, x2) and between(yp, y1, y2),
        do: [{xp, yp} | possibles],
        else: possibles

    possible_obstructions(p1, p2, t, new_possibles)
  end

  defp slope({x1, _}, {x2, _}) when x2 - x1 == 0, do: :inf

  defp slope({x1, y1}, {x2, y2}) do
    (y2 - y1) / (x2 - x1)
  end

  defp quadrant(p1, p2) do
    case {p1, p2} do
      {{x1, y1}, {x2, y2}} when x2 >= x1 and y2 < y1 -> 1
      {{x1, y1}, {x2, y2}} when x2 > x1 and y2 >= y1 -> 2
      {{x1, y1}, {x2, y2}} when x2 <= x1 and y2 > y1 -> 3
      {{x1, y1}, {x2, y2}} when x2 < x1 and y2 <= y1 -> 4
    end
  end

  defp on_line(t, p1, p2) do
    s1 = slope(p1, p2)
    s2 = slope(p1, t)
    s3 = slope(t, p2)
    s1 == s2 and s2 == s3
  end

  defp line_of_sight(p, p, _), do: false

  defp line_of_sight(fst, lst, obstructions) do
    possible_obstructions(fst, lst, obstructions, [])
    |> Enum.all?(fn t -> not on_line(t, fst, lst) end)
  end

  defp in_los(asteroids, p1) do
    Enum.split_with(asteroids, fn p2 ->
      line_of_sight(p1, p2, asteroids)
    end)
  end

  defp calc_slopes(asteroids, p1) do
    Enum.map(asteroids, fn p2 ->
      slope = slope(p1, p2)
      quadrant = quadrant(p1, p2)
      {p2, quadrant, slope}
    end)
    |> Enum.sort(fn
      {_, 1, s}, {_, 1, s2} -> s == :inf or (s2 != :inf and s < s2)
      {_, 2, s}, {_, 2, s2} -> s < s2
      {_, 3, s}, {_, 3, s2} -> s == :inf or (s2 != :inf and s < s2)
      {_, 4, s}, {_, 4, s2} -> s < s2
      {_, q, _s}, {_, q2, _s2} -> q < q2
    end)
  end

  def count_visible(field) do
    asteroids = locate_asteroids(field)

    Enum.map(asteroids, fn p1 ->
      sum =
        Enum.map(asteroids, fn p2 ->
          if line_of_sight(p1, p2, asteroids), do: 1, else: 0
        end)
        |> Enum.sum()

      {p1, sum}
    end)
  end

  defp order_by_laser([], _, results), do: results

  defp order_by_laser(rest, p, results) do
    {los, remains} = in_los(rest, p)

    if length(los) > 0,
      do:
        (
          new_slopes = calc_slopes(los, p)
          order_by_laser(remains, p, Enum.concat(results, new_slopes))
        ),
      else: results
  end

  def order_by_laser(field, p) do
    locate_asteroids(field)
    |> order_by_laser(p, [])
  end
end
