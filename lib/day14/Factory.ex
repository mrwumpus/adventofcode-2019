defmodule Factory do
  use GenServer

  def start_link(initial) do
    state =
      String.trim(initial)
      |> String.split("\n")
      |> Enum.map(fn line ->
        String.split(line, " => ")
      end)
      |> Enum.reduce(%{}, fn [l, r], acc ->
        deps =
          String.split(l, ", ")
          |> Enum.map(fn dep ->
            [num, sym] = String.split(dep, " ")
            {String.to_integer(num), sym}
          end)

        [cnt, tgt] = String.split(r, " ")

        Map.put(acc, tgt, {String.to_integer(cnt), deps})
      end)

    # IO.inspect(state)

    GenServer.start_link(__MODULE__, state)
  end

  def state(pid) do
    GenServer.call(pid, :state)
  end

  def min_ore(pid, fuels \\ 1) do
    GenServer.call(pid, {:min_ore, fuels})
  end

  def fuel_max(pid, ore) do
    GenServer.call(pid, {:max_fuel, ore})
  end

  @impl true
  def init(state) do
    {:ok, state}
  end

  @impl true
  def handle_call(:state, _from, state) do
    {:reply, state, state}
  end

  @impl true
  def handle_call({:min_ore, fuels}, _from, state) do
    {_, min} = min_ore(state, %{}, "FUEL", fuels)
    {:reply, min, state}
  end

  @impl true
  def handle_call({:max_fuel, ore}, _from, state) do
    {min, max} = find_range(state, 1, ore)
    max = ore_in_range(state, ore, min, max)
    {:reply, max, state}
  end

  @impl true
  def handle_call(_message, _from, state) do
    {:ok, state}
  end

  defp find_range(state, fuel, tgt_ore) do
    interval = 1_000_000
    {_, cur_ore} = min_ore(state, %{}, "FUEL", fuel)

    if cur_ore < tgt_ore,
      do: find_range(state, fuel + interval, tgt_ore),
      else: {fuel - interval, fuel}
  end

  defp ore_in_range(_, _, fuel, fuel), do: fuel

  defp ore_in_range(state, tgt_ore, fuel_min, fuel_max) do
    fuel = fuel_min + div(fuel_max - fuel_min, 2) + 1
    #IO.puts("Checking between #{fuel_min} and #{fuel_max} at midpoint #{fuel}")
    {_, cur_ore} = min_ore(state, %{}, "FUEL", fuel)

    if cur_ore > tgt_ore,
      do: ore_in_range(state, tgt_ore, fuel_min, fuel - 1),
      else: ore_in_range(state, tgt_ore, fuel, fuel_max)
  end

  defp min_ore(_, remains, "ORE", required) do
    # IO.puts("Oh, you need #{required} ORE")
    {remains, required}
  end

  defp min_ore(_, remains, _, 0), do: {remains, 0}

  defp min_ore(state, remains, key, required) do
    {count, deps} = Map.get(state, key)
    available = Map.get(remains, key, 0)

    # IO.puts("Require #{required} #{key}, have #{available} in reserve")

    if available < required,
      do:
        (
          mult = ceil((required - available) / count)
          new_remaining = available + (mult * count - required)
          new_remains = Map.put(remains, key, new_remaining)

          # IO.puts("Got #{mult*count} more, now have #{new_remaining} in reserve")

          Enum.reduce(deps, {new_remains, 0}, fn {sup, dep_key}, {tot_remains, tot_required} ->
            {ret_remains, ret_required} = min_ore(state, tot_remains, dep_key, sup * mult)
            {ret_remains, tot_required + ret_required}
          end)
        ),
      else:
        (
          new_remaining = available - required
          # IO.puts("Had enough, now have #{new_remaining} in reserve")
          new_remains = Map.put(remains, key, new_remaining)
          {new_remains, 0}
        )
  end
end
