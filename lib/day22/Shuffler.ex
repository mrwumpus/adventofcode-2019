defmodule Shuffler do
  defp new_stack(deck), do: Enum.reverse(deck)

  defp cut(deck, cards) do
    {front, back} = Enum.split(deck, cards)
    Enum.concat(back, front)
  end

  defp inc_shuffle([], deck, _, _), do: deck

  defp inc_shuffle([c | rest], deck, idx, inc) do
    i = rem(idx + inc, length(deck))
    inc_shuffle(rest, List.replace_at(deck, idx, c), i, inc)
  end

  defp increment(deck, inc) do
    #IO.puts("Increment, length #{length(deck)} at #{inc}")
    inc_shuffle(deck, List.duplicate(0, length(deck)), 0, inc)
  end

  defp process_line(deck, "deal into new stack"), do: new_stack(deck)

  defp process_line(deck, "cut " <> cards) do
    trim =
      String.trim(cards)
      |> String.to_integer()

    cut(deck, trim)
  end

  defp process_line(deck, "deal with increment " <> inc) do
    trim =
      String.trim(inc)
      |> String.to_integer()

    increment(deck, trim)
  end

  def shuffle(deck, shuffle),
    do:
      String.split(shuffle, "\n", trim: true)
      |> Enum.reduce(deck, fn shuffle, deck ->
        #IO.puts(shuffle)
        process_line(deck, shuffle)
      end)
end
