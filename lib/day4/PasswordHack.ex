defmodule PasswordHack do
  @moduledoc """
  Day 4, password hacking
  """
  defp number_check(0, _, results), do: results
  # short circuit when number is not not descending
  defp number_check(_, _, {false, _}), do: {false, %{}}

  defp number_check(number, nil, _) do
    next = div(number, 10)
    digit = rem(number, 10)
    number_check(next, digit, {true, %{digit => 1}})
  end

  defp number_check(number, last, {decreases, counts}) do
    next = div(number, 10)
    digit = rem(number, 10)

    new_counts = Map.update(counts, digit, 1, &(&1 + 1))
    still_decreases = decreases and last >= digit
    result = {still_decreases, new_counts}

    number_check(next, digit, result)
  end

  defp doubles_check(true), do: fn count -> count == 2 end
  defp doubles_check(false), do: fn count -> count > 1 end

  def number_check(number, pure_doubles \\ false) do
    {decreases, counts} = number_check(number, nil, nil)
    decreases and Map.values(counts) |> Enum.any?(doubles_check(pure_doubles))
  end

  def count_range(start, end_range, total, pure_doubles \\ false)
  def count_range(start, end_range, total, _) when start > end_range, do: total

  def count_range(start, end_range, total, pure_doubles) do
    check = if number_check(start, pure_doubles), do: 1, else: 0
    count_range(start + 1, end_range, total + check, pure_doubles)
  end
end
