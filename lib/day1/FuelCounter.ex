defmodule FuelCounter do
  @moduledoc """
  First Day of AOC
  """

  def calc_fuel(mass), do: div(mass, 3) - 2

  defp do_fuel_calc(mass, total) when mass > 0,
    do: do_fuel_calc(calc_fuel(mass), mass + total)

  defp do_fuel_calc(_, total), do: total

  def calc_fuel_including_fuel(mass), do: calc_fuel(mass) |> do_fuel_calc(0)

  def calc_fuel_totals(body) do
    masses = String.split(body) |> Enum.map(&String.to_integer/1)

    total = masses |> Enum.map(&calc_fuel/1) |> Enum.sum()
    full_total = masses |> Enum.map(&calc_fuel_including_fuel/1) |> Enum.sum()
    {total, full_total}
  end
end
