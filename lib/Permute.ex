defmodule Permute do
  @moduledoc"""
  Make some permutations
  """
  def permute([n]), do: [[n]]
  def permute(list) do
    list
    |> Enum.flat_map(fn n ->
      permute(List.delete(list, n))
      |> Enum.map(fn lst -> [n | lst] end)
    end)
  end
end
