defmodule Camera do
  use GenServer

  def start_link() do
    GenServer.start_link(__MODULE__, [])
  end

  def view(pid) do
    GenServer.call(pid, :state)
  end

  @impl true
  def init(state) do
    {:ok, state}
  end

  @impl true
  def handle_call(:state, _from, state) do
    {:reply, state, state}
  end

  @impl true
  def handle_info(msg, state) do
    input = String.to_integer(msg)
    {:noreply, [input | state]}
  end

end
