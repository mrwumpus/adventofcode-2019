defmodule Intcode5Test do
  use ExUnit.Case
  doctest Intcode5


  test "Day 2 Regression tests" do
    assert Intcode5.run_code("1,0,0,0,99") === "2,0,0,0,99"
    assert Intcode5.run_code("2,3,0,3,99") === "2,3,0,6,99"
    assert Intcode5.run_code("2,4,4,5,99,0") === "2,4,4,5,99,9801"
    assert Intcode5.run_code("1,1,1,4,99,5,6,0,99") === "30,1,1,4,2,5,6,0,99"

    assert Intcode5.run_code("1,9,10,3,2,3,11,0,99,30,40,50") ===
             "3500,9,10,70,2,3,11,0,99,30,40,50"
  end

  test "part 2 examples" do
    # Intcode5.run_code("3,9,8,9,10,9,4,9,99,-1,8")
  end

  def check_with_io(input, opcodes) do
    IoAgent.start_link()

    StringIO.open("", fn o_pid ->
      IoAgent.set_stdout(o_pid)

      StringIO.open(input, fn i_pid ->
        IoAgent.set_stdin(i_pid)
        Intcode5.run_code(opcodes)
      end)

      StringIO.contents(o_pid)
      |> (fn {_, result} -> result end).()
      |> String.trim()
      |> String.split()
      |> List.last()
      |> String.to_integer()
    end)
  end

  test "Solve the problem" do
    input_file = "test/day5/input"

    opcodes =
      case File.read(input_file) do
        {:ok, body} ->
          body

        {:error, reason} ->
          IO.puts("Oops -- #{reason}")
          :error
      end

    assert opcodes != :error

    input_1 = "1"
    {:ok, output_for_1} = check_with_io(input_1, opcodes)
    input_5 = "5"
    {:ok, output_for_5} = check_with_io(input_5, opcodes)

    IO.puts("""

      DAY 5
      -=-=-=-=-=-=-=-=-=-=-=
      INPUT:  #{input_1} 
      OUTPUT: #{output_for_1}
      
      INPUT:  #{input_5} 
      OUTPUT: #{output_for_5} 
    """)

    assert output_for_1 == 7_692_125
    assert output_for_5 == 14_340_395
  end
end

