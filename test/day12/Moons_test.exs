defmodule MoonsTest do
  use ExUnit.Case

  test "solve it" do
    input = Util.input_day(12)
    assert input != :error

    {:ok, pid} = Moons.start_link(input)
    %{:moons => moons} = Moons.simulate(pid, 1000)
    GenServer.stop(pid)

    e =
      Enum.map(moons, fn {{x, y, z}, {dx, dy, dz}} ->
        (abs(x) + abs(y) + abs(z)) *
          (abs(dx) + abs(dy) + abs(dz))
      end)
      |> Enum.sum()

    #{:ok, pid} = Moons.start_link(input)
    #gens = Moons.find_loop(pid)
    #GenServer.stop(pid)

    IO.puts("""

     DAY 12
     -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
      TOTAL ENERGY: #{e}
      GENS TO LOOP: ???
    """)

    assert e == 9139

  end
end
