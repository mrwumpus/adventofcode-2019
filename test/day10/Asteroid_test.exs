defmodule AsteroidTest do
  use ExUnit.Case

  defp max_coords(field) do
    Asteroid.count_visible(field)
    |> Enum.reduce(fn {p, count}, {max_p, max_count} ->
      if count > max_count, do: {p, count}, else: {max_p, max_count}
    end)
  end

  test "First example" do
    field = """
    ......#.#.
    #..#.#....
    ..#######.
    .#.#.###..
    .#..#.....
    ..#....#.#
    #..#....#.
    .##.#..###
    ##...#..#.
    .#....####
    """

    {{x, y}, count} = max_coords(field)

    assert {x, y} == {5, 8}
    assert count == 33
  end

  test "second example" do
    field = """
    #.#...#.#.
    .###....#.
    .#....#...
    ##.#.#.#.#
    ....#.#.#.
    .##..###.#
    ..#...##..
    ..##....##
    ......#...
    .####.###.
    """

    {{x, y}, count} = max_coords(field)

    assert {x, y} == {1, 2}
    assert count == 35
  end

  test "third example" do
    field = """
    .#..#..###
    ####.###.#
    ....###.#.
    ..###.##.#
    ##.##.#.#.
    ....###..#
    ..#.#..#.#
    #..#.#.###
    .##...##.#
    .....#.#..
    """

    {{x, y}, count} = max_coords(field)

    assert {x, y} == {6, 3}
    assert count == 41
  end

  test "fourth example" do
    field = """
    .#..##.###...#######
    ##.############..##.
    .#.######.########.#
    .###.#######.####.#.
    #####.##.#.##.###.##
    ..#####..#.#########
    ####################
    #.####....###.#.#.##
    ##.#################
    #####.##.###..####..
    ..######..##.#######
    ####.##.####...##..#
    .#####..#.######.###
    ##...#.##########...
    #.##########.#######
    .####.#.###.###.#.##
    ....##.##.###..#####
    .#.#.###########.###
    #.#.#.#####.####.###
    ###.##.####.##.#..##
    """

    {{x, y}, count} = max_coords(field)

    assert {x, y} == {11, 13}
    assert count == 210
  end

  test "solve part one" do
    input = Util.input_day(10)

    assert input != :error

    {{x, y}, count} = max_coords(input)

    IO.puts("""

     DAY 10 
     -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
     POINT:       (#{x},#{y})
     COUNT:       #{count}
    """)

    assert count == 292
    assert x == 20
    assert y == 20
  end

  test "pt 2 example" do
    field = """
    .#..##.###...#######
    ##.############..##.
    .#.######.########.#
    .###.#######.####.#.
    #####.##.#.##.###.##
    ..#####..#.#########
    ####################
    #.####....###.#.#.##
    ##.#################
    #####.##.###..####..
    ..######..##.#######
    ####.##.####...##..#
    .#####..#.######.###
    ##...#.##########...
    #.##########.#######
    .####.#.###.###.#.##
    ....##.##.###..#####
    .#.#.###########.###
    #.#.#.#####.####.###
    ###.##.####.##.#..##
    """

    results =
      Asteroid.order_by_laser(field, {11, 13})
      |> Enum.map(fn {{x, y}, _, _} ->
        x * 100 + y
      end)

    assert Enum.at(results, 0) == 1112
    assert Enum.at(results, 1) == 1201
    assert Enum.at(results, 2) == 1202
    assert Enum.at(results, 9) == 1208
    assert Enum.at(results, 99) == 1016
    assert Enum.at(results, 198) == 906 
    assert Enum.at(results, 199) == 802 
    assert Enum.at(results, 200) == 1009 
    assert List.last(results) == 1101 
  end

  test "solve part 2" do
    input = Util.input_day(10)

    assert input != :error

    results = Asteroid.order_by_laser(input, {20, 20})
    {{x200, y200}, _, _} = Enum.at(results, 199)

    result = x200 * 100 + y200

    assert result != 1719

    IO.puts("""

     DAY 10, p2
     -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
     200th coord: #{result}
    """)
  end
end
