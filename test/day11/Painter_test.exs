defmodule PainterTest do
  use ExUnit.Case

  def run_code(intcode, out_pid) do
    {:ok, pid} = IntcodeState11.start_link(intcode, out_pid)
    task = Task.async(fn -> Intcode11.run_code(pid) end)

    Painter.set_ouput_pid(out_pid, task.pid)

    result = Task.await(task, 30_000)
    Agent.stop(pid)
    result
  end

  def extents(nodes) do
    Map.keys(nodes)
    |> Enum.reduce({nil, nil}, fn
      p, {nil, nil} ->
        {p, p}

      {x, y}, {{min_x, min_y}, {max_x, max_y}} ->
        {{min(x, min_x), min(y, min_y)}, {max(x, max_x), max(y, max_y)}}
    end)
  end

  test "run painter" do
    input = Util.input_day(11)
    assert input != :error

    {:ok, paint_pid} = Painter.start_link()
    run_code(input, paint_pid)
    %{:nodes => nodes} = Painter.complete(paint_pid)

    node_size = map_size(nodes)

    {:ok, paint_pid} = Painter.start_link(1)
    run_code(input, paint_pid)
    %{:nodes => nodes} = Painter.complete(paint_pid)

    {{min_x, min_y}, {max_x, max_y}} = extents(nodes)

    pic =
      for y <- max_y..min_y,
          x <- min_x..max_x do
        Map.get(nodes, {x, y}, 0)
      end
      |> Enum.map(fn c ->
        if c == 0, do: " ", else: "#"
      end)
      |> Enum.chunk_every(max_x - min_x + 1)
      |> Enum.map(fn line ->
        Enum.join(line)
      end)
      |> Enum.join("\n")


    IO.puts("""

     DAY 11
     -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
      PAINTED TILES: #{node_size}
      
      LICENSE:
    #{pic}
    """)

    assert node_size == 2021
  end
end
