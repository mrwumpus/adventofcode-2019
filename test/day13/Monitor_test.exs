defmodule MonitorTest do
  use ExUnit.Case

  test "hook up monitor" do
    input = Util.input_day(13)
    {:ok, monitor_pid} = Monitor.start_link()

    {:ok, config_pid} = IntcodeState11.start_link(input, monitor_pid)
    Intcode11.run_code(config_pid)

    {_, _, block_count} = Monitor.ball_paddle_blocks(monitor_pid)

    Agent.stop(config_pid)
    GenServer.stop(monitor_pid)

    IO.puts("""
     Day 13, pt 1
     ._._._._._._._._._._.
     BLOCKS: #{block_count}
    """)

    assert block_count == 361
  end

  def play_loop(ball, paddle, 0, _, _) when not is_nil(ball) and not is_nil(paddle) do
    nil
  end

  def play_loop(ball, paddle, _, task, monitor_pid) do
    {cur_ball, cur_paddle, cur_blocks} = Monitor.ball_paddle_blocks(monitor_pid)

    # if blocks != nil and cur_blocks < blocks do
    # score =
    # Monitor.screen(monitor_pid)
    # |> Map.get({-1, 0}, 0)

    # IO.puts("#{cur_blocks} remain, score: #{score}")
    # end

    if cur_ball != ball do
      {cur_x, _} = cur_ball

      instruction =
        cond do
          cur_x == paddle -> 0
          paddle < cur_x -> 1
          paddle > cur_x -> -1
        end

      send(task.pid, instruction)
    end

    play_loop(cur_ball, cur_paddle, cur_blocks, task, monitor_pid)
  end

  test "win the game" do
    input = Util.input_day(13)
    {:ok, monitor_pid} = Monitor.start_link()
    input = String.replace_prefix(input, "1,", "2,")

    {:ok, config_pid} = IntcodeState11.start_link(input, monitor_pid)

    task = Task.async(fn -> Intcode11.run_code(config_pid) end)

    play_loop(nil, nil, nil, task, monitor_pid)
    Process.sleep(1000)

    score =
      Monitor.screen(monitor_pid)
      |> Map.get({-1, 0}, 0)

    IO.puts("""
     Day 13, pt 2
     ._._._._._._._._._._.
     SCORE: #{score}
    """)

    Task.shutdown(task)
    Agent.stop(config_pid)
    GenServer.stop(monitor_pid)

    assert score == 17_590
  end
end
