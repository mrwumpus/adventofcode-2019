defmodule OrbitGraphTest do
  use ExUnit.Case
  doctest OrbitGraph

  setup do
    {result, _} = Orbits.start_link()
    result
  end

  defp tokenize_input(input) do
    input |> String.split()
  end

  test "part 1 example" do
    input = """
    COM)B
    B)C
    C)D
    D)E
    E)F
    B)G
    G)H
    D)I
    E)J
    J)K
    K)L
    """

    tokenize_input(input) |> Enum.each(fn token ->
      OrbitGraph.add_orbit(token)
    end)

    steps = OrbitGraph.walk_orbits()
    assert steps == 42
  end

  test "part 2 example" do
    input = """
    COM)B
    B)C
    C)D
    D)E
    E)F
    B)G
    G)H
    D)I
    E)J
    J)K
    K)L
    K)YOU
    I)SAN
    """

    tokenize_input(input) |> Enum.each(fn token ->
      OrbitGraph.add_orbit(token)
    end)

    steps_between = OrbitGraph.steps_between("YOU", "SAN")

    assert length(steps_between) - 1 == 4
  end

  test "solve the day" do
    input_file = "test/day6/input"

    input =
      case File.read(input_file) do
        {:ok, body} ->
          body

        {:error, reason} ->
          IO.puts("Oops -- #{reason}")
          :error
      end

    assert input != :error

    tokenize_input(input) |> Enum.each(fn token ->
      OrbitGraph.add_orbit(token)
    end)

    steps = OrbitGraph.walk_orbits()

    steps_between = OrbitGraph.steps_between("YOU", "SAN")

    IO.puts("""

      DAY 6
      -=-=-=-=-=-=-=-=-=-=-=
      DIRECT, INDIRECT ORBITS: #{steps}
      STEPS BETWEEN:           #{length(steps_between) - 1}
    """)

    assert steps == 224_901
    assert length(steps_between) - 1 == 334

  end
end
