defmodule FuelCounterTest do
  use ExUnit.Case
  doctest FuelCounter

  test "check examples" do
    assert FuelCounter.calc_fuel(12) == 2
    assert FuelCounter.calc_fuel(14) == 2
    assert FuelCounter.calc_fuel(1969) == 654
    assert FuelCounter.calc_fuel(100_756) == 33_583
  end

  test "check recursive examples" do
    assert FuelCounter.calc_fuel_including_fuel(14) == 2
    assert FuelCounter.calc_fuel_including_fuel(1969) == 966
    assert FuelCounter.calc_fuel_including_fuel(100_756) == 50_346
  end

  test "Determine the fuel count" do
    input_file = "test/day1/input"
    masses = case File.read(input_file) do
      {:ok, body} -> body
      {:error, reason} -> IO.puts("Oops -- #{reason}")
    end

    assert masses

    {total, full_total} = FuelCounter.calc_fuel_totals(masses)

    IO.puts("""

      DAY 1
      =-=-=-=-=-=-=-=-=-=-=-=-=
      TOTAL:      #{total}
      FULL TOTAL: #{full_total}
    """)

    assert total == 3_263_354
    assert full_total == 4_892_166
  end
end
