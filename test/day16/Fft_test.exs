defmodule FftTest do
  use ExUnit.Case

  test "examples" do
    assert Fft.process_signal("12345678", 4) ==
             "01029498"

    assert Fft.process_signal("80871224585914546619083218645595", 100)
           |> String.slice(0..7) == "24176176"

    assert Fft.process_signal("19617804207202209144916044189917", 100)
           |> String.slice(0..7) == "73745418"

    assert Fft.process_signal("69317163492948606335995924319873", 100)
           |> String.slice(0..7) == "52432133"
  end

  test "solution" do
    input = Util.input_day(16)
    assert input != :error

    val =
      Fft.process_signal(String.trim(input), 100)
      |> String.slice(0..7)

    # brute_force = 
    # String.trim(input)
    # |> List.duplicate(10_000)
    # |> Enum.join()
    # |> Fft.process_signal(100)

    # brute_force_offset = 
    # String.slice(brute_force, 0..6)
    # |> String.to_integer()

    # brute_force_value = String.slice(brute_force, brute_force_offset, 8)

    IO.puts("""

     DAY 16
     -=-=-=-=-=-=-=-==-=-
     VALUE: #{val}
    """)

    assert val == "73127523"
  end
end
