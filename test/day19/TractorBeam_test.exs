defmodule TractorBeamTest do
  use ExUnit.Case

  defp test_point(input, camera_pid, x, y) do
    {:ok, config_pid} = IntcodeState11.start_link(input, camera_pid)
    task = Task.async(fn -> Intcode11.run_code(config_pid) end)
    send(task.pid, x)
    send(task.pid, y)
    Task.await(task)
  end

  test "map the beam" do
    input = Util.input_day(19)

    assert input != :error

    {:ok, camera_pid} = Camera.start_link()

    for y <- 0..49,
        x <- 0..49 do
      # IO.puts("Sending (#{x},#{y})")
      test_point(input, camera_pid, x, y)
    end

    view = Camera.view(camera_pid)

    count = Enum.count(view, fn v -> v > 0 end)

    view =
      Enum.map(view, fn v -> if v > 0, do: "#", else: "." end)
      |> Enum.reverse()
      |> Enum.chunk_every(50)
      |> Enum.map(fn l -> Enum.join(l, "") end)
      |> Enum.join("\n")

    IO.puts(view)

    IO.puts("""
    Day 19
    oOoOoOoOoOoOoOoOoOo
    COUNT: #{count}
    """)

    assert count == 162

    GenServer.stop(camera_pid)
  end

  def get_eval() do
    input = Util.input_day(19)

    assert input != :error

    fn
      x, y when x < 0 or y < 0 ->
        "."

      x, y ->
        {:ok, camera_pid} = Camera.start_link()
        {:ok, config_pid} = IntcodeState11.start_link(input, camera_pid)
        task = Task.async(fn -> Intcode11.run_code(config_pid) end)
        send(task.pid, x)
        send(task.pid, y)
        Task.await(task)

        result =
          Camera.view(camera_pid)
          |> List.first()

        GenServer.stop(camera_pid)
        Agent.stop(config_pid)
        result
    end
  end

  test "search the beam" do
    x = BeamTracker.find_beam(get_eval(), 300, 500)

    {x, y} = BeamTracker.search_for_room(get_eval(), 99, x, 500)

    result = 10_000 * x + y

    IO.puts("""
     Day 19, pt 2
     oOoOoOoOoOoOoOoOoOo
     RESULT: #{result}
    """)

    assert result = 13_021_056
  end
end
