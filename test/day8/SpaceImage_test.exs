defmodule SpaceImageTest do
  use ExUnit.Case
  doctest SpaceImage

  test "example" do
    {:ok, pid} = SpaceImageConfig.start_link("123456789012", 3, 2)

    assert SpaceImage.layers(pid) == ["123456", "789012"]
    assert SpaceImage.layer_with_fewest(pid, 0) == "123456"
    Agent.stop(pid)
  end

  test "solve it" do
    input = Util.input_day(8)

    assert input != :error

    {:ok, pid} = SpaceImageConfig.start_link(String.trim(input), 25, 6)

    layer = SpaceImage.layer_with_fewest(pid, 0)
    ohs = SpaceImage.count_in_layer(layer, 0)
    ones = SpaceImage.count_in_layer(layer, 1)
    twos = SpaceImage.count_in_layer(layer, 2)

    assert ohs + ones + twos == String.length(layer)

    checksum = ones * twos

    message = SpaceImage.render_message(pid)

    IO.puts("""

     DAY 8
     -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
     CHECKSUM: #{ones * twos}
     MESSAGE:
    #{message}
    """)

    assert checksum == 1905

    assert message <> "\n" == """
            ##   ##  #  # ###  #### 
           #  # #  # # #  #  #    # 
           #  # #    ##   #  #   #  
           #### #    # #  ###   #   
           #  # #  # # #  #    #    
           #  #  ##  #  # #    #### 
           """

    Agent.stop(pid)
  end
end
