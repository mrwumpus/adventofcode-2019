defmodule CameraTest do
  use ExUnit.Case

  defp at(lines, x, y) do
    ch =
      Enum.at(lines, y, "")
      |> String.at(x)

    if ch, do: ch, else: "."
  end

  defp n(lines, x, y), do: at(lines, x, y - 1)
  defp e(lines, x, y), do: at(lines, x + 1, y)
  defp s(lines, x, y), do: at(lines, x, y + 1)
  defp w(lines, x, y), do: at(lines, x - 1, y)

  defp intersection(lines, x, y) do
    # IO.puts("Checking: #{x}, #{y}: #{at(lines, x, y)}")

    # IO.puts(
    # "(#{x},#{y} : #{at(lines, x, y)}) -- N: #{n(lines, x, y)}, E: #{e(lines, x, y)}, S: #{
    # s(lines, x, y)
    # }, W: #{w(lines, x, y)}"
    # )

    at(lines, x, y) == "#" and
      n(lines, x, y) == "#" and
      e(lines, x, y) == "#" and
      s(lines, x, y) == "#" and
      w(lines, x, y) == "#"
  end

  def score(lines) do
    view_lines =
      lines
      |> String.split("\n", trim: true)

    for y <- 0..(length(view_lines) - 1),
        x <- 0..(String.length(List.first(view_lines)) - 1),
        _ = intersection(view_lines, x, y) do
      {x, y}
    end
    |> Enum.reduce(0, fn {a, b}, sum -> a * b + sum end)
  end

  test "example" do
    input = """
    ..#..........
    ..#..........
    #######...###
    #.#...#...#.#
    #############
    ..#...#...#..
    ..#####...^..
    """

    sum = score(input)

    assert sum == 76
  end

  test "read the input" do
    input = Util.input_day(17)

    assert input != :error

    {:ok, camera_pid} = Camera.start_link()
    {:ok, config_pid} = IntcodeState11.start_link(input, camera_pid)

    task = Task.async(fn -> Intcode11.run_code(config_pid) end)
    Task.await(task, 30_000)

    view =
      Camera.view(camera_pid)
      |> Enum.reverse()

    assert is_list(view)

    sum =
      List.to_string(view)
      |> score()

    Agent.stop(config_pid)
    GenServer.stop(camera_pid)

    IO.puts("""
    day 17, pt 1
    |||||||||||||||||||
    SUM: #{sum}
    """)

    assert sum < 9144
  end

  test "robot program" do
    program = """
    A,B,A,C,B,C,B,C,A,C
    L,10,R,12,R,12
    R,6,R,10,L,10
    R,10,L,10,L,12,R,6
    n
    """

    input = Util.input_day(17)

    assert input != :error
    input = String.replace_prefix(input, "1,", "2,")

    {:ok, camera_pid} = Camera.start_link()
    {:ok, config_pid} = IntcodeState11.start_link(input, camera_pid)

    task = Task.async(fn -> Intcode11.run_code(config_pid) end)

    String.to_charlist(program)
    |> Enum.each(fn ch ->
      send(task.pid, ch)
    end)

    Task.await(task, 30_000)

    result =
      Camera.view(camera_pid)
      |> List.first()

    IO.puts("""
    day 17, pt 2
    |||||||||||||||||||
    DUSTY!: #{result}
    """)

    assert result == 927_809
  end
end
