defmodule PasswordHackTest do
  use ExUnit.Case
  doctest PasswordHack

  test "problem examples" do
    assert PasswordHack.number_check(111_111) == true
    assert PasswordHack.number_check(223_450) == false
    assert PasswordHack.number_check(123_789) == false
  end

  test "part 2 examples" do
    assert PasswordHack.number_check(111_111, true) == false
    assert PasswordHack.number_check(223_450, true) == false
    assert PasswordHack.number_check(123_789, true) == false
    assert PasswordHack.number_check(112_233, true) == true
    assert PasswordHack.number_check(123_444, true) == false
    assert PasswordHack.number_check(111_122, true) == true
  end

  test "solve the problem" do
    total = PasswordHack.count_range(178_416, 676_461, 0)
    pure_total = PasswordHack.count_range(178_416, 676_461, 0, true)

    IO.puts("""

      DAY 4
      -=-=-=-=-=-=-=-=-=-=-=-=-=-
      TOTAL:              #{total}
      PURE DOUBLES TOTAL: #{pure_total}
    """)

    assert total == 1650
    assert pure_total == 1129
  end

end
