defmodule WirePanelTest do
  use ExUnit.Case
  doctest WirePanel

  test "problem examples" do
    assert WirePanel.minimum_manhattan([
             "R8,U5,L5,D3",
             "U7,R6,D4,L4"
           ]) == 6

    assert WirePanel.minimum_manhattan([
             "R75,D30,R83,U83,L12,D49,R71,U7,L72",
             "U62,R66,U55,R34,D71,R55,D58,R83"
           ]) == 159

    assert WirePanel.minimum_manhattan([
             "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51",
             "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"
           ]) == 135
  end

  test "part 2 examples" do
    assert WirePanel.minimum_intersection_steps([
             "R8,U5,L5,D3",
             "U7,R6,D4,L4"
           ]) == 30 

    assert WirePanel.minimum_intersection_steps([
             "R75,D30,R83,U83,L12,D49,R71,U7,L72",
             "U62,R66,U55,R34,D71,R55,D58,R83"
           ]) == 610

    assert WirePanel.minimum_intersection_steps([
             "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51",
             "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"
           ]) == 410
  end

  test "Solve the problem" do
    input_file = "test/day3/input"

    wire_strs =
      case File.read(input_file) do
        {:ok, body} -> body
        {:error, reason} -> IO.puts("Oops -- #{reason}")
      end
      |> String.trim()
      |> String.split()

    assert wire_strs

    minimum = WirePanel.minimum_manhattan(wire_strs)

    min_steps = WirePanel.minimum_intersection_steps(wire_strs)

    IO.puts("""

      DAY 3
      -=-=-=-=-=-=-=-=-=-=-=
      MIN. MANHATTAN: #{minimum}
      MIN. STEPS SUM: #{min_steps}
    """)

    assert minimum == 273
    assert min_steps == 15_622

  end
end
