defmodule ShufflerTest do
  use ExUnit.Case

  test "example shuffles" do
    shuffle = """
    deal with increment 7
    deal into new stack
    deal into new stack
    """

    deck = 0..9 |> Enum.map(fn v -> v end)
    shuffled = Shuffler.shuffle(deck, shuffle)

    assert shuffled == [0, 3, 6, 9, 2, 5, 8, 1, 4, 7]
  end

  test "example number two" do
    shuffle = """
    cut 6
    deal with increment 7
    deal into new stack
    """

    deck = 0..9 |> Enum.map(fn v -> v end)
    shuffled = Shuffler.shuffle(deck, shuffle)

    assert shuffled == [3, 0, 7, 4, 1, 8, 5, 2, 9, 6]
  end

  test "example number three" do
    shuffle = """
    deal with increment 7
    deal with increment 9
    cut -2
    """

    deck = 0..9 |> Enum.map(fn v -> v end)
    shuffled = Shuffler.shuffle(deck, shuffle)

    assert shuffled == [6, 3, 0, 7, 4, 1, 8, 5, 2, 9]
  end

  test "example number last" do
    shuffle = """
    deal into new stack
    cut -2
    deal with increment 7
    cut 8
    cut -4
    deal with increment 7
    cut 3
    deal with increment 9
    deal with increment 3
    cut -1
    """

    deck = 0..9 |> Enum.map(fn v -> v end)
    shuffled = Shuffler.shuffle(deck, shuffle)

    assert shuffled == [9, 2, 5, 8, 1, 4, 7, 0, 3, 6]
  end

  test "do the day" do
    input = Util.input_day(22)

    assert input != :error

    deck = 0..10_006 |> Enum.map(fn v -> v end)
    shuffled = Shuffler.shuffle(deck, input)

    pos =
      shuffled
      |> Enum.find_index(fn v -> v == 2019 end)

    IO.puts("""
     Day 22
     %%%%%%%%%%%%%%%%%
     CARD 2019 @: #{pos}
    """)
  end

  # lol
  #test "do the day, part 2" do
    #input = Util.input_day(22)

    #assert input != :error

    #giga_deck =
      #0..119_315_717_514_046
      #|> Enum.map(fn v -> v end)

    #giga_shuffle = Shuffler.shuffle(giga_deck, input)

    #IO.puts("""
     #Day 22, pt 2
     #%%%%%%%%%%%%%%%%%
      #???
    #""")
  #end
end
