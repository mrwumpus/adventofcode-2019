defmodule JumpbotTest do
  use ExUnit.Case

  defp run_program(intcode, springscript) do
    {:ok, camera_pid} = Camera.start_link()
    {:ok, config_pid} = IntcodeState11.start_link(intcode, camera_pid)

    task = Task.async(fn -> Intcode11.run_code(config_pid) end)

    String.to_charlist(springscript)
    |> Enum.each(fn ch -> send(task.pid, ch) end)

    Task.await(task, 120_000)

    result = Camera.view(camera_pid)

    GenServer.stop(camera_pid)
    Agent.stop(config_pid)

    result
  end

  @tag timeout: 120_000
  test "Do the jumping" do
    input = Util.input_day(21)

    assert input != :error

    walk_program = """
    NOT C T
    AND D T
    OR T J
    NOT A T
    OR T J
    WALK
    """

    view = run_program(input, walk_program)
    damage = List.first(view)

    run_program = """
    NOT C T
    AND D T
    AND H T
    OR T J
    NOT B T
    AND D T
    OR T J
    NOT A T
    OR T J
    RUN
    """

    view = run_program(input, run_program)

    run_damage = List.first(view)

    IO.puts("""
     Day 21
     #################
     WALK DAMAGE: #{damage}
     RUN DAMAGE: #{run_damage}
    """)

    assert damage == 19_352_638
    assert run_damage == 1_141_251_258
  end
end
