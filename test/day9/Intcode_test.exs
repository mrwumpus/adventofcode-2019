defmodule Intcode9Test do
  use ExUnit.Case
  doctest Intcode9

  def run_code(intcode, input \\ [], device \\ :stdio) do
    {:ok, pid} = IntcodeState.start_link(intcode, device)
    task = Task.async(fn -> Intcode9.run_code(pid) end)

    Enum.each(input, fn val ->
      send(task.pid, val)
    end)

    result = Task.await(task, 30_000)
    Agent.stop(pid)
    result
  end

  def run_code_with_output(intcode, input \\ []) do
    {:ok, {_, output}} =
      StringIO.open("", fn pid ->
        result = run_code(intcode, input, pid)
        {"", output} = StringIO.contents(pid)
        {result, output}
      end)

    String.trim(output)
  end

  test "Day 2 Regression tests" do
    assert run_code("1,0,0,0,99") === "2,0,0,0,99"
    assert run_code("2,3,0,3,99") === "2,3,0,6,99"
    assert run_code("2,4,4,5,99,0") === "2,4,4,5,99,9801"
    assert run_code("1,1,1,4,99,5,6,0,99") === "30,1,1,4,2,5,6,0,99"

    assert run_code("1,9,10,3,2,3,11,0,99,30,40,50") ===
             "3500,9,10,70,2,3,11,0,99,30,40,50"
  end

  test "Day 9 examples" do
    assert run_code_with_output("104,1125899906842624,99") == "1125899906842624"
    assert run_code_with_output("1102,34915192,34915192,7,4,7,99,0") == "1219070632396864"
    quine = "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99"

    assert run_code_with_output(quine)
           |> String.split()
           |> Enum.join(",") == quine
  end

  test "solution" do
    input = Util.input_day(9)

    assert input != :error

    result = run_code_with_output(input, [1])
    boost =  run_code_with_output(input, [2])

    IO.puts("""

     DAY 9
     -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
      CHECKSUM: #{result}
      BOOST:    #{boost}
    """)

    assert result == "2941952859"
    assert boost == "66113"
  end
end
