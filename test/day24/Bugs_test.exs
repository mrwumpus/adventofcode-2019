defmodule BugsTest do
  use ExUnit.Case

  defp trim_input(input),
    do:
      input
      |> String.replace("\n", "")

  test "check examples" do
    initial =
      """
      ....#
      #..#.
      #..##
      ..#..
      #....
      """
      |> trim_input()

    dup = Bugs.find_dup(initial, 5, 5)

    Bugs.print(dup, 5)

    bio = Bugs.biodiversity(dup)

    assert bio == 2_129_920
  end

  test "find answers" do
    input = Util.input_day(24)

    assert input != :error

    initial = trim_input(input)

    dup = Bugs.find_dup(initial, 5, 5)
    Bugs.print(dup, 5)
    bio = Bugs.biodiversity(dup)

    IO.puts("""
       DAY 24
       =-=-=-=-=-=-=-=-=-=-=-=-
       BIODIVERSITY: #{bio}
      """)
  end
end
