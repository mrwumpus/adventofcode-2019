defmodule IntcodeTest do
  use ExUnit.Case
  doctest Intcode2

  test "problem examples" do
    assert Intcode2.run_code("1,0,0,0,99") === "2,0,0,0,99"
    assert Intcode2.run_code("2,3,0,3,99") === "2,3,0,6,99"
    assert Intcode2.run_code("2,4,4,5,99,0") === "2,4,4,5,99,9801"
    assert Intcode2.run_code("1,1,1,4,99,5,6,0,99") === "30,1,1,4,2,5,6,0,99"

    assert Intcode2.run_code("1,9,10,3,2,3,11,0,99,30,40,50") ===
             "3500,9,10,70,2,3,11,0,99,30,40,50"
  end

  test "Solve the problem" do
    input_file = "test/day2/input"

    opcodes =
      case File.read(input_file) do
        {:ok, body} -> body
        {:error, reason} -> (
            IO.puts("Oops -- #{reason}")
            :error
        )
      end

    assert opcodes != :error

    result = Intcode2.run_code(opcodes, 12, 2)
    first = Intcode2.get_num(result, 0)

    [{noun, verb}] = Intcode2.search_result(opcodes, 19_690_720)

    assert noun > -1 && noun < 100
    assert verb > -1 && verb < 100

    IO.puts("""

      DAY 2
      -=-=-=-=-=-=-=-=-=-=-=
      VALUE AT 0:    #{first}
      NOUN and VERB: #{100 * noun + verb}
    """)

    assert first == 3_516_593

    assert noun == 77
    assert verb == 49
  end
end
